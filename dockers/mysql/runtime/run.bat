@echo off

set PWD=%~dp0
REM echo %PWD%
rem docker network create dbnet
docker run --name e7mysql -d --restart always --network dbnet -p 3306:3306 -v %~dp0/logs:/opt/mysql/logs -v %~dp0/data:/opt/mysql/data -e MYSQL_ALLOW_EMPTY_PASSWORD=yes e7sky/mysql:5.6.39
REM docker run --name e7mysql --rm --network dbnet -p 3306:3306 -v %PWD%data:/opt/mysql/data -e MYSQL_ALLOW_EMPTY_PASSWORD=yes e7sky/mysql:5.6.39
