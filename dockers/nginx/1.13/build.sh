#!/bin/bash

PWD=$(cd $(dirname ${BASH_SOURCE[0]}); pwd )

docker build -f $PWD/Dockerfile -t e7sky/nginx:1.13.9 .