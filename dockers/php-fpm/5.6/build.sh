#!/bin/bash

PWD=$(cd $(dirname ${BASH_SOURCE[0]}); pwd )

docker build -f $PWD/Dockerfile -t e7sky/php-fpm:5.6.33 .