###  e7dockers 是一个由源码安装 Nginx+PHP-FPM+MySQL 的 Dockerfile 仓库，旨在快速构建基于Docker的LNMP环境。



> 特性： 

- 1，源码安装，安装细节清晰，定制更方便
- 2，Nginx, PHP-FPM基于 alpine:3.7 构建，体积更小
- 3，Nginx, PHP-FPM, MySQL 以独立用户运行，权限清晰，配置文件独立，完全定制
- 4，通过镜像分层，提高镜像复用性

> 版本说明：

- OpenSSL: 1.0.2j
- PHP: 5.6.33(with imagick-3.4.3 & ImageMagick-6.9.9-34)、7.0.27、7.1.14、7.2.2
- Nginx: 1.10.2、1.11.13、1.12.2、1.13.9
- MySQL: 5.6.39

> 使用方式：

**step1 构建镜像**
```bash
cd <e7dockers dir>

sudo ./dockers/alpine/3.7/build.sh
sudo ./dockers/was/3.7/build.sh
sudo ./dockers/nginx/1.10/build.sh
sudo ./dockers/php-fpm/5.6/build.sh
sudo ./dockers/mysql/5.6/build.sh
```
这里可能得需要一段时间，因为需要编译程序包


**step2 查看镜像**
```bash
sudo docker images
```
如果没有错的话，上面的命令会显示出来刚刚构建好的镜像


**step3 运行镜像**
```bash
sudo docker-compose up
```


**step4 测试**
```bash
sudo docker-compose ps
```
通过上面的命令查看容器运行状态。如一切顺利，则可通过访问 http://localhost:8080与http://localhost:8080/phpinfo.php 查看是否运行成功。

> 后续计划：
- 1, pholcus 容器化
- 2, 增加mysql 5.7、8
- 3, 增加MariaDB

> 最后：  
欢迎提建议指正，不定时更新中。。。。。By Joen > <kai168@126.com>
