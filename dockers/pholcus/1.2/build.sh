#!/bin/bash

PWD=$(cd $(dirname ${BASH_SOURCE[0]}); pwd )

docker build -f $PWD/Dockerfile -t e7sky/pholcus:1.2 .